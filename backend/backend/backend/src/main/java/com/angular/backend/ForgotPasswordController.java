package com.angular.backend;

import com.model.User;
import com.dao.UserRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/forgot-password")
@CrossOrigin(origins = "http://localhost:4200")
public class ForgotPasswordController {	

    private final UserRepository userRepository;
    private final OtpService otpService;

    @Autowired
    public ForgotPasswordController(UserRepository userRepository, OtpService otpService) {
        this.userRepository = userRepository;
        this.otpService = otpService;
    }		

   


    @PostMapping("/send-otp")
    public ResponseEntity<?> sendOtp(@RequestBody Map<String, String> requestBody) {
        String mobileNumber = requestBody.get("mobileNumber");

        if (mobileNumber == null) {
            return ResponseEntity.badRequest().body("Mobile number is required for password reset");
        }

        User user = userRepository.findByMobileNumber(mobileNumber);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found for the given mobile number");
        }

        // Generate OTP and send it to the user's mobile number
        String otp = otpService.generateOtp(mobileNumber);
        otpService.sendOtp(user, otp);

        return ResponseEntity.ok("OTP sent successfully");
    }

    @PostMapping("/verify-otp")
    public ResponseEntity<?> verifyOtp(@RequestBody Map<String, String> requestBody) {
        String mobileNumber = requestBody.get("mobileNumber");
        String enteredOtp = requestBody.get("otp");

        if (mobileNumber == null || enteredOtp == null) {
            return ResponseEntity.badRequest().body("Mobile number and OTP are required for verification");
        }

        String storedOtp = otpService.getOtp(mobileNumber);

        if (storedOtp != null && enteredOtp.equals(storedOtp)) {
            otpService.clearOtp(mobileNumber);
            return ResponseEntity.ok("{\"message\":\"OTP verification successful\"}");
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{\"error\":\"Incorrect OTP or expired\"}");
        }
    }

    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody Map<String, String> requestBody) {
        String mobileNumber = requestBody.get("mobileNumber");
        String newPassword = requestBody.get("newPassword");

        if (mobileNumber == null || newPassword == null) {
            return ResponseEntity.badRequest().body("Mobile number and new password are required for reset");
        }

        User user = userRepository.findByMobileNumber(mobileNumber);

        if (user != null) {
            System.out.println("User found: " + user.getEmailId()); // Debugging
            System.out.println("Mobile Number: " + mobileNumber); // Debugging
            System.out.println("New Password: " + newPassword); // Debugging

            // Hash the new password
            String hashedPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt(12));

            // Set the hashed password
            user.setPassword(hashedPassword);

            // Save the user
            userRepository.save(user);

            // Clear OTP
            otpService.clearOtp(mobileNumber);

            return ResponseEntity.ok("{\"message\":\"Password reset successful\"}");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\"error\":\"User not found\"}");
        }
    }
}
