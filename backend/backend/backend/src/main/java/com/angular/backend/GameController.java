package com.angular.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.GameRequestDao;
import com.dao.UserDao;
import com.model.GameRequest;
import com.model.User;

import java.util.Optional;

@RestController
public class GameController {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private GameRequestDao gameRequestDao;

    @Autowired
    private UserDao userDao;

    @PostMapping("/sendGameRequest/{userEmail}")
    public ResponseEntity<String> sendGameRequest(@RequestBody GameRequest gameRequest, @PathVariable String userEmail) {
        try {
            // Check if the user exists in the User table
            Optional<User> existingUser = userDao.getUserByEmail(userEmail);

            if (existingUser.isPresent()) {

                GameRequest newGameRequest = new GameRequest(
                        gameRequest.getGameName(),
                        gameRequest.getReleaseYear(),
                        gameRequest.getPublisher(),
                        userEmail
                );

                newGameRequest.setUser(existingUser.get());

                // Save the game request
                gameRequestDao.saveGameRequest(newGameRequest);

                // Prepare the email message
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo("studiousvishnu@gmail.com");
                message.setSubject("Game Request");
                message.setText("Game Name: " + newGameRequest.getGameName() +
                        "\nRelease Year: " + newGameRequest.getReleaseYear() +
                        "\nPublisher: " + newGameRequest.getPublisher() +
                        "\nUser Email: " + newGameRequest.getUserEmail());

                // Send the email
                emailSender.send(message);

                // Return a response with a success message
                return ResponseEntity.status(HttpStatus.CREATED).body("Game request sent, saved, and email sent successfully!");
            } else {
                // User not found, return a response with an error status code (e.g., 404 Not Found)
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found. Failed to send and save game request.");
            }
        } catch (Exception e) {
            // Return a response with an error message
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to send, save, and email game request. Error: " + e.getMessage());
        }
    }
}
