	package com.angular.backend;
	
	import com.model.User;
	import com.dao.UserRepository;
	import org.mindrot.jbcrypt.BCrypt;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.http.ResponseEntity;
	import org.springframework.mail.MailException;
	import org.springframework.mail.SimpleMailMessage;
	import org.springframework.mail.javamail.JavaMailSender;
	import org.springframework.web.bind.annotation.*;
	import com.fasterxml.jackson.core.JsonProcessingException;
	import com.fasterxml.jackson.databind.ObjectMapper;

	
	@RestController
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	@CrossOrigin(origins = "http://localhost:4200") 
	public class UserController {
	
	    private final UserRepository userRepository;
	    private final JavaMailSender javaMailSender;
	
	    @Autowired
	    public UserController(UserRepository userRepository, JavaMailSender javaMailSender) {
	        this.userRepository = userRepository;
	        this.javaMailSender = javaMailSender;
	    }
	
	    @PostMapping
	    public ResponseEntity<?> signUp(@RequestBody User user) {
	        // Log the received data
	        System.out.println("Received Signup: " + user);
	
	        // Validate required fields
	        if (user.getEmailId() == null || user.getPassword() == null) {
	            return ResponseEntity.badRequest().body("EmailId and password are required");
	        }
	
	        // Check if the email already exists
	        User existingSignup = userRepository.findByEmailId(user.getEmailId());
	
	        if (existingSignup != null) {
	            return ResponseEntity.status(409).body("Email already exists in the database");
	        }
	
	        // Hash the password using jBCrypt
	        String hashedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12));
	
	        // Set the hashed password back to the signup object
	        user.setPassword(hashedPassword);
	
	        // Save the signup with the hashed password
	        userRepository.save(user);
	
	        // Send registration success email
	        sendRegistrationEmail(user.getEmailId());
	
	        // Return a JSON response with a success message
	        return ResponseEntity.ok().body("{\"message\":\"Signup successful!\"}");
	    }
	
	    @GetMapping("/getsignupByEmailIdAndPassword/{emailId}/{password}")
	    public ResponseEntity<?> getSignupByEmailIdAndPassword(
	            @PathVariable("emailId") String emailId,
	            @PathVariable("password") String password) {

	        User user = userRepository.findByEmailId(emailId);

	        if (user != null && BCrypt.checkpw(password, user.getPassword())) {
	            // User signed in successfully
	            // Convert the user details to a JSON string
	            ObjectMapper objectMapper = new ObjectMapper();
	            String userJson;
	            try {
	                userJson = objectMapper.writeValueAsString(user);
	            } catch (JsonProcessingException e) {
	                return ResponseEntity.status(500).body("Error converting user details to JSON");
	            }

	            return ResponseEntity.ok(userJson);
	        } else {
	            // Invalid email or password
	            return ResponseEntity.status(401).body("Invalid email or password");
	        }
	    }

	    private void sendRegistrationEmail(String email) {
	        try {
	            SimpleMailMessage message = new SimpleMailMessage();
	            message.setTo(email);
	            message.setSubject("Registration Successful!!!");
	            message.setText("Thank you for joining GameSpot! Enjoy your purchases!!!! Keep Gaming!!!" );
	            
	            
	
	            javaMailSender.send(message);
	        } catch (MailException e) {
	            // Log or handle the exception
	            e.printStackTrace();
	        }
	    }
	}
