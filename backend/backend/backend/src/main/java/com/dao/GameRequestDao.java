package com.dao;

import com.model.GameRequest;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class GameRequestDao {

    @Autowired
    private GameRequestRepository gameRequestRepository;

    @Autowired
    private UserDao userDao;

    public boolean saveUserIfNotPresent(User user) {
        // Check if the user already exists
        if (!userDao.getUserByEmail(user.getEmailId()).isPresent()) {
            userDao.save(user);
            return true;
        }
        return false;
    }

    public void saveGameRequest(GameRequest gameRequest) {
        gameRequestRepository.save(gameRequest);
    }
}
