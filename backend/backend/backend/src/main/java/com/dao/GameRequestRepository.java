package com.dao;

import com.model.GameRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRequestRepository extends JpaRepository<GameRequest, Long> {
    // Additional methods can be added if needed
}
