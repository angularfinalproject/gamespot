package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDao {
    @Autowired
    private UserRepository userRepo;

    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    public Optional<User> getUserByEmail(String email) {
        // Change the method to return Optional<User> directly
        return Optional.ofNullable(userRepo.findByEmailId(email));
    }

    public void save(User user) {
        userRepo.save(user);
    }
}
