package com.angular.backend;

import com.config.EmailConfig;
import com.config.TwilioConfig;
import com.config.CorsConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.dao")
@EntityScan(basePackages = "com.model")
@ComponentScan(basePackages = {"com.angular.backend", "com.config", "com.dao", "com.model"})
@Import({EmailConfig.class, TwilioConfig.class, CorsConfig.class})
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }
}
