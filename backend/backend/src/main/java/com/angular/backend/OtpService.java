package com.angular.backend;

import com.dao.UserRepository;
import com.model.User;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class OtpService {

    @Value("${twilio.phoneNumber}")
    private String twilioPhoneNumber;

    private final Map<String, String> otpMap = new HashMap<>();

    private final UserRepository userRepository;

    @Autowired
    public OtpService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String generateOtp(String mobileNumber) {
        // Generate a random 6-digit OTP
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        String otpString = String.valueOf(otp);

        // Save the OTP for later verification
        otpMap.put(mobileNumber, otpString);

        return otpString;
    }

    public void sendOtp(User user, String otp) {
        // Use Twilio to send SMS with OTP
        Message message = Message.creator(
                new PhoneNumber(user.getMobileNumber()),  // Use the user's mobile number
                new PhoneNumber(twilioPhoneNumber),
                "Your OTP for password reset: " + otp)
                .create();

        System.out.println("OTP sent. Message SID: " + message.getSid());
    }

    public String getOtp(String mobileNumber) {
        return otpMap.get(mobileNumber);
    }

    public boolean verifyOtp(String mobileNumber, String enteredOtp) {
        // Verify the entered OTP against the stored OTP
        String storedOtp = otpMap.get(mobileNumber);
        return storedOtp != null && storedOtp.equals(enteredOtp);
    }

    public void updatePassword(String mobileNumber, String newPassword) {
        // Retrieve the user by mobile number
        User user = userRepository.findByMobileNumber(mobileNumber);

        // Update the user's password
        if (user != null) {
            user.setPassword(newPassword);
            userRepository.save(user);
        }
    }

    public void clearOtp(String mobileNumber) {
        otpMap.remove(mobileNumber);
    }
}
