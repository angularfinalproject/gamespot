package com.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> 
{	User findByEmailIdAndPassword(String emailId, String password);
    User findByEmailId(String emailId);
    User findByMobileNumber(String mobileNumber);

}
