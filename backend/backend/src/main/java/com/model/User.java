package com.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sno")
    private int SNO;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email_id", unique = true, nullable = false)
    private String emailId;


    @Column(name = "password")
    private String password;

    @Column(name = "mobile_number", unique = true, nullable = false)
    private String mobileNumber; User() {
	        super();
	    }
	
	    public User(String fullName, String emailId, String password) {
	        super();
	        this.fullName = fullName;
	        this.emailId = emailId;
	        this.password = password;
	        
	    }
	
	    public int getSNO() {
	        return SNO;
	    }
	
	    public void setSNO(int sNO) {
	        SNO = sNO;
	    }
	
	    public String getFullName() {
	        return fullName;
	    }
	
	    public void setFullName(String fullName) {
	        this.fullName = fullName;
	    }
	
	    public String getEmailId() {
	        return emailId;
	    }
	
	    public void setEmailId(String emailId) {
	        this.emailId = emailId;
	    }
	
	    public String getPassword() {
	        return password;
	    }
	
	    public void setPassword(String password) {
	        this.password = password;
	    }
	    
	    public String getMobileNumber() {
	        return mobileNumber;
	    }

	    public void setMobileNumber(String mobileNumber) {
	        this.mobileNumber = mobileNumber;
	    }
	
	    public String toString() {
	        return "Signup[SNO=" + SNO + ",fullName=" + fullName + ",email=" + emailId + ",password=" + password + ",mobileNumber=" + mobileNumber + "]";
	    }


}
