import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NewYearSaleComponent } from './new-year-sale/new-year-sale.component';
import { ChristmasSaleComponent } from './christmas-sale/christmas-sale.component';
import { BrowseGames2Component } from './browse-games-2/browse-games-2.component';
import { BrowseGamesComponent } from './browse-games/browse-games.component';
import { Game1Component } from './game-1/game-1.component';
import { FilterComponent } from './filter/filter.component';
import { CartComponent } from './cart/cart.component';
import { FiltercartComponent } from './filtercart/filtercart.component';
import { AuthService } from './auth.service';
import { CheckoutComponent } from './checkout/checkout.component';
import { AuthGuard } from './authguard.service';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password', component: ForgotpasswordComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'browse', component: BrowseGamesComponent},
  { path: 'browse2', component: BrowseGames2Component},
  { path: 'newyearsale', component: NewYearSaleComponent},
  { path: 'christmassale', component: ChristmasSaleComponent},
  { path: 'game1', component: Game1Component },
  { path: 'cart', component: CartComponent, canActivate: [AuthGuard] },
  { path: 'filter', component: FilterComponent, canActivate: [AuthGuard] },
  { path: 'filtercart', component: FiltercartComponent, canActivate: [AuthGuard] },
  { path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
