import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Import FormsModule
import { RouterModule } from '@angular/router';
import { NgToastModule } from 'ng-angular-popup';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { AuthService } from './auth.service';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NewYearSaleComponent } from './new-year-sale/new-year-sale.component';
import { ChristmasSaleComponent } from './christmas-sale/christmas-sale.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { BrowseGamesComponent } from './browse-games/browse-games.component';
import { BrowseGames2Component } from './browse-games-2/browse-games-2.component';
import { FooterComponent } from './footer/footer.component';
import { Game1Component } from './game-1/game-1.component';
import { FilterComponent } from './filter/filter.component';
import { CartComponent } from './cart/cart.component';
import { FiltercartComponent } from './filtercart/filtercart.component';
import { AuthGuard } from './authguard.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    HeaderComponent,
    ForgotpasswordComponent,
    AboutusComponent,
    ContactusComponent,
    NewYearSaleComponent,
    ChristmasSaleComponent,
    CheckoutComponent,
    BrowseGamesComponent,
    BrowseGames2Component,
    FooterComponent,
    Game1Component,
    FilterComponent,
    CartComponent,
    FiltercartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, // Add this line to import FormsModule
    HttpClientModule,
    RouterModule, //for routing
    NgToastModule,
    ReactiveFormsModule,
    FontAwesomeModule,

  ],
  providers: [AuthService,
  AuthGuard],
  bootstrap: [AppComponent]

})

export class AppModule { }
