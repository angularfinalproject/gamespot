// auth.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = 'http://localhost:8085'; // Update with your server API URL
  private isLoggedIn = false;

  constructor(private http: HttpClient) {}

  login(email: string, password: string): Observable<any> {
    return this.http.get(`${this.apiUrl}/signup/getsignupByEmailIdAndPassword/${email}/${password}`, { responseType: 'text' });
  }

  logout(): void {
    // Implement your logout logic here
    this.isLoggedIn = false;
  }

  isAuthenticated(): boolean {
    return this.isLoggedIn;
  }

  setAuthenticated(value: boolean): void {
    this.isLoggedIn = value;
  }
}
