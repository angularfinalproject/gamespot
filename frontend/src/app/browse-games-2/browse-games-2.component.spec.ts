import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseGames2Component } from './browse-games-2.component';

describe('BrowseGames2Component', () => {
  let component: BrowseGames2Component;
  let fixture: ComponentFixture<BrowseGames2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BrowseGames2Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BrowseGames2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
