import { Component, OnInit } from '@angular/core';
import { GamespotService } from '../gamespot.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-browse-games',
  templateUrl: './browse-games.component.html',
  styleUrl: './browse-games.component.css'
})
export class BrowseGamesComponent implements OnInit {

  cards = [
    
      {text:"Base Game",title:"Assasin's Creed Mirage",discount:"-55%",orginalprice:"3,499",saleprice:1574,imgsrc:"../../assets/images/allgames/img-1.png",backgroundimage:"../../assets/images/background/assassins-creed-3840x2560-13046.jpg",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:1080,count:1},
      {text:"Base Game",title:"Spiderman 2",discount:"-57%",orginalprice:"4,574",saleprice:2059,imgsrc:"../../assets/images/allgames/img-2.png",backgroundimage:"../../assets/images/background/game_spiderman.jpg",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:7,gcard:2080,count:1},
      {text:"Base Game",title:"Alan Wake 2",discount:"-50%",orginalprice:"3,999",saleprice:1999,imgsrc:"../../assets/images/allgames/img-3.png",backgroundimage:"../../assets/images/background/game_3.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:9,gcard:3080,count:1},
      {text:"Base Game",title:"Baldurs Gate 3",discount:"-55%",orginalprice:"3,499",saleprice:2999,imgsrc:"../../assets/images/allgames/img_23.png",backgroundimage:"../../assets/images/background/game_4.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:2080,count:1},
      {text:"Base Game",title:"Call of Duty",discount:"-55%",orginalprice:"4,500",saleprice:2540,imgsrc:"../../assets/images/allgames/img_6.png",backgroundimage:"../../assets/images/background/game_6.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:9,gcard:3080,count:1},
      {text:"Base Game",title:"GTA 5",discount:"-55%",orginalprice:"4,500",saleprice:1600,imgsrc:"../../assets/images/allgames/img_28.png",backgroundimage:"../../assets/images/background/game_gta.jpg",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:7,gcard:1080,count:1},
      {text:"Base Game",title:"Elden Ringe",discount:"-55%",orginalprice:"5,500",saleprice:3600,imgsrc:"../../assets/images/allgames/img_11.png",backgroundimage:"../../assets/images/background/game_elden.jpg",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:3080,count:1},
      {text:"Base Game",title:"Sekiro™: Shadows Die Twice",discount:"-55%",orginalprice:"4,500",saleprice:2600,imgsrc:"../../assets/images/allgames/img-11.png",backgroundimage:"../../assets/images/background/game_7.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:9,gcard:2080,count:1},
      {text:"Base Game",title:"Red Dead Redemption 2",discount:"-50%",orginalprice:"4,900",saleprice:3600,imgsrc:"../../assets/images/allgames/img-14.png",backgroundimage:"../../assets/images/background/game_red.jpg",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:7,gcard:1080,count:1},
      {text:"Base Game",title:"Mortal Kombat 1",discount:"-45%",orginalprice:"5,500",saleprice:2200,imgsrc:"../../assets/images/allgames/img-15.png",backgroundimage:"../../assets/images/background/game_15.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:9,gcard:3080,count:1},
      {text:"Base Game",title:"Battlefield 2024",discount:"-55%",orginalprice:"4,500",saleprice:2600,imgsrc:"../../assets/images/allgames/img-17.png",backgroundimage:"../../assets/images/background/game_16.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:9,gcard:2080,count:1},
      {text:"Base Game",title:"Dead Island 2",discount:"-15%",orginalprice:"9,500",saleprice:8600,imgsrc:"../../assets/images/allgames/img-18.png",backgroundimage:"../../assets/images/background/game_18.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:9,gcard:1080},
      {text:"Base Game",title:"UNCHARTED 4: A Thief’s End",discount:"-70%",orginalprice:"4,500",saleprice:1300,imgsrc:"../../assets/images/allgames/img-19.png",backgroundimage:"../../assets/images/background/game_19.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:7,gcard:3080,count:1},
      {text:"Base Game",title:"The Witcher 3: Wild Hunt",discount:"-65%",orginalprice:"8,500",saleprice:3600,imgsrc:"../../assets/images/allgames/img-20.png",backgroundimage:"../../assets/images/background/game_witcher.jpg",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:7,gcard:1080,count:1},
      {text:"Base Game",title:"God of War:Ragnarok",discount:"-35%",orginalprice:"4,500",saleprice:1600,imgsrc:"../../assets/images/allgames/img-12.png",backgroundimage:"../../assets/images/background/game_12.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:7,gcard:1080,count:1},
      {text:"Base Game",title:"Cyberpunk 2077",discount:"-45%",orginalprice:"7,500",saleprice:4600,imgsrc:"../../assets/images/allgames/img-11.png",backgroundimage:"../../assets/images/background/game_11.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:1080,count:1},
      {text:"Base Game",title:"The Crew:Moto Fast",discount:"-55%",orginalprice:"4,500",saleprice:2600,imgsrc:"../../assets/images/allgames/img-10.png",backgroundimage:"../../assets/images/background/game_10.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:2080,count:1},
      {text:"Base Game",title:"UFC-5",discount:"-65%",orginalprice:"6,500",saleprice:3600,imgsrc:"../../assets/images/allgames/img-9.png",backgroundimage:"../../assets/images/background/game_9.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:3080,count:1},
      {text:"Base Game",title:"Far Cry 6",discount:"-35%",orginalprice:"3,500",saleprice:1430,imgsrc:"../../assets/images/allgames/img-8.png",backgroundimage:"../../assets/images/background/game_8.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:3080,count:1},
      {text:"Base Game",title:"Last Of Us 2 ",discount:"-45%",orginalprice:"2,500",saleprice:1190,imgsrc:"../../assets/images/allgames/img_21.png",backgroundimage:"../../assets/images/background/game_5.png",prod:"UBISOFT ENTERTAINMENT SA",rating:"4.4",ratingnumber:"12k",date: "Offer ends 6/1/2024 05:29 AM IST+5:30",imgsrc1:"../../assets/images/background/game_1.png",imgsrc2:"../../assets/images/background/mirage (2).jpg",memory:8,storage:40,intel:5,gcard:3080,count:1},







  ];
  ngOnInit() {

  }
  email:any
  singleCard: any[]=[];
  constructor(private service: GamespotService,private router: Router) {
    this.email=localStorage.getItem('emailId')

  }

  click(item:any){
    this.singleCard.push(item);
    localStorage.setItem( `cartItems${this.email}`, JSON.stringify(this.singleCard));
    this.router.navigate(['game1']);


  }

}
