import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  filteredData:any
  localStorageData:any;
  localStorageCount:any;
  email:any;
  total:any;
  cartEmpty:any;
  count:any
  constructor(private toast: NgToastService){
    this.email=localStorage.getItem('emailId')
    console.log(this.email)
   this.localStorageData=localStorage.getItem(`addcart${this.email}`);
   this.filteredData=JSON.parse(this.localStorageData)

   this.localStorageCount=localStorage.getItem(`totalPrices${this.email}`);
   this.total=JSON.parse(this.localStorageCount);
   console.log("thisprice:"+this.total)

   const storedTotalCount = localStorage.getItem(`totalCount${this.email}`);
   this.count = storedTotalCount ? parseFloat(storedTotalCount) : 0;

  }
  deleteCart(item:any){
    this.toast.success({
      detail: 'SUCCESS', summary: 'Deleted item from cart successfully',
      duration: 2000, position: 'topCenter'
    });

    localStorage.removeItem(`addcart${this.email}`);
    this.filteredData = this.filteredData.filter((dataItem: any) => dataItem !== item);
    localStorage.setItem(`addcart${this.email}`,JSON.stringify(this.filteredData));
    console.log(item)
    if (item && item.saleprice) {
      this.count-=item.count;
      this.total -= item.saleprice;
      localStorage.setItem(`totalPrices${this.email}`, JSON.stringify(this.total));
      localStorage.setItem(`totalCount${this.email}`, JSON.stringify(this.count));

    }
    if (this.filteredData.length === 0) {
    localStorage.removeItem(`totalPrices${this.email}`);
    localStorage.removeItem(`totalCount${this.email}`);

    this.total = null; // Assuming you also want to reset the total in your component
    this.count=null;
    console.log('Total prices cleared from local storage');
    this.cartEmpty="your is empty"

    }

    }


  ngOnInit() {
  }

}
