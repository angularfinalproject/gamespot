// checkout.component.ts

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent {
  checkoutForm: FormGroup;

  constructor(private fb: FormBuilder, private toast: NgToastService,private router: Router) {
    this.checkoutForm = this.fb.group({
      cardholderName: ['', [Validators.required, Validators.minLength(3)]],
      cardNumber: ['', [Validators.required, Validators.pattern('[0-9]{9}')]],
      cardType: ['', Validators.required],
      expiry: ['', Validators.required],
      cvv: ['', [Validators.required, Validators.pattern('[0-9]{3}')]],
    });
  }

  validateForm() {
    console.log('Validating form...'); // Add a log to check if the method is being called
    if (this.checkoutForm.valid) {
     // Add a temporary alert for debugging
      this.toast.success({
        detail: 'SUCCESS',
        summary: 'Payment success',
        duration: 2000,
        position: 'topCenter',
      });




      // You can perform additional actions on successful form submission
    } else {

      this.showToast('Please fill out the required fields correctly');
    }
  }

  showToast(message: string) {
    // Implement your toast functionality here
    // You can use a library or Angular Material Snackbar for this purpose
    console.log(message);
  }click(){
    this.validateForm();
    this.router.navigate(['home']);
  }

}
