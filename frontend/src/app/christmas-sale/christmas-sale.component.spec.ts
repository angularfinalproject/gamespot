import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChristmasSaleComponent } from './christmas-sale.component';

describe('ChristmasSaleComponent', () => {
  let component: ChristmasSaleComponent;
  let fixture: ComponentFixture<ChristmasSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChristmasSaleComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChristmasSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
