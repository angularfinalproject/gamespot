import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltercartComponent } from './filtercart.component';

describe('FiltercartComponent', () => {
  let component: FiltercartComponent;
  let fixture: ComponentFixture<FiltercartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FiltercartComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FiltercartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
