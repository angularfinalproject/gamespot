import { Component, OnInit } from '@angular/core';
import { GamespotService } from '../gamespot.service';
import { NgToastService } from 'ng-angular-popup';


@Component({
  selector: 'app-filtercart',
  templateUrl: './filtercart.component.html',
  styleUrl: './filtercart.component.css'
})
export class FiltercartComponent  implements OnInit {
  email: any;
  cartItems: any;
  addcart: any[] = [];
  totalprice: number = 0;
  filterItems: any;
  totalcount:number=0;
  constructor(private service: GamespotService,private toast: NgToastService) {
    this.email = localStorage.getItem('emailId');
    const addedCart = localStorage.getItem(`addcart${this.email}`);
    this.addcart = addedCart ? JSON.parse(addedCart) : [];
    const storedFilterItems = localStorage.getItem(`filterItems${this.email}`);
    this.filterItems = storedFilterItems ? JSON.parse(storedFilterItems) : [];
    const storedTotalPrice = localStorage.getItem(`totalPrices${this.email}`);
    this.totalprice = storedTotalPrice ? parseFloat(storedTotalPrice) : 0;
    const storedTotalCount = localStorage.getItem(`totalCount${this.email}`);
    this.totalcount = storedTotalCount ? parseFloat(storedTotalCount) : 0;
  }




  ngOnInit() {}

  addCart(item: any) {
    const existingItem = this.addcart.find((cartItem: any) => cartItem.title === item.title);
    if (existingItem) {
      this.toast.warning({
        detail: 'WARNING', summary: 'Item already added ',
        duration: 2000, position: 'topCenter'
      });
      this.totalcount=this.totalcount;
    } else {
      this.toast.success({
        detail: 'SUCCESS', summary: 'Item added to cart successfully',
        duration: 2000, position: 'topCenter'
      });
      this.addcart.push(item);
      this.totalcount=this.totalcount+item.count;
      this.totalprice = this.totalprice + item.saleprice;
      localStorage.setItem(`totalPrices${this.email}`, this.totalprice.toString());
      localStorage.setItem(`totalCount${this.email}`, this.totalcount.toString());
      localStorage.setItem(`addcart${this.email}`, JSON.stringify(this.addcart));

    }
  }
}
