import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private http: HttpClient) { }

  sendOtp(mobileNumber: any): Observable<any> {
    return this.http.post('http://localhost:8085/forgot-password/send-otp', { mobileNumber }, { responseType: 'text' });
  }

  verifyOtp(mobileNumber: string, otp: any): Observable<any> {
    return this.http.post('http://localhost:8085/forgot-password/verify-otp', { mobileNumber, otp }, { headers: { 'Content-Type': 'application/json' } });
  }

  updatePassword(mobileNumber: string, newPassword: string): Observable<any> {
    const data = { mobileNumber, newPassword };
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post('http://localhost:8085/forgot-password/reset-password', data, { headers });
  }

  getAllCountriesCodes():any{
    return this.http.get("https://restcountries.com/v3.1/all");
  }

}
