// forgotpassword.component.ts
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { ForgotPasswordService } from '../forgot-password.service';
import { NgToastService } from 'ng-angular-popup';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
})
export class ForgotpasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  currentStep: 'sendOtp' | 'verifyOtp' | 'updatePassword' = 'sendOtp';
  countries: any;

  constructor(
    private fb: FormBuilder,
    private forgotPasswordService: ForgotPasswordService,
    private toast: NgToastService,
    private router: Router
  ) {
    this.forgotPasswordForm = this.fb.group({
      countryCode: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required, this.mobileNumberValidator]], // Added custom validator
      otp: ['', [Validators.required]],

      newPassword: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          this.passwordComplexityValidator(),
        ],
      ],
      confirmPassword: ['', Validators.required],
    }, { validators: this.passwordMatchValidator });
  }

  ngOnInit() {
    // Fetch country codes when the component initializes
    this.forgotPasswordService.getAllCountriesCodes().subscribe((data: any) => {
      this.countries = data;
    });
  }

  sendOtp() {
    const countryCodeControl = this.forgotPasswordForm.get('countryCode');
    const mobileNumberControl = this.forgotPasswordForm.get('mobileNumber');

    if (!countryCodeControl || !mobileNumberControl || countryCodeControl.value === '' || mobileNumberControl.value === '') {
      this.toast.error({
        detail: '',
        summary: 'Country code and mobile number cannot be empty',
        duration: 2000,
        position: 'topCenter',
      });
      return;
    }

    const countryCode: string = countryCodeControl.value;
    const mobileNumber: string = mobileNumberControl.value;
    const fullPhoneNumber = countryCode + mobileNumber;

    console.log(fullPhoneNumber);

    this.forgotPasswordService.sendOtp(fullPhoneNumber).subscribe(
      (response) => {
        this.toast.success({
          detail: '',
          summary: response,
          duration: 2000,
          position: 'topCenter',
        });
        this.currentStep = 'verifyOtp';
      },
      (error) => {
        console.error('Error sending OTP:', error);
        this.handleError(error, 'Failed to send OTP');
      }
    );
  }

  verifyOtp() {
    const countryCode = this.forgotPasswordForm.value.countryCode;
    const mobileNumber = this.forgotPasswordForm.value.mobileNumber;
    const otpControl = this.forgotPasswordForm.get('otp');

    if (!otpControl || otpControl.value === '' || countryCode === '' || mobileNumber === '') {
      this.toast.error({
        detail: '',
        summary: 'OTP field cannot be empty',
        duration: 2000,
        position: 'topCenter',
      });
      return;
    }

    const otp = otpControl.value;
    const fullPhoneNumber = countryCode + mobileNumber;

    this.forgotPasswordService.verifyOtp(fullPhoneNumber, otp).subscribe(
      () => {
        this.toast.success({
          detail: '',
          summary: 'OTP verified successfully',
          duration: 2000,
          position: 'topCenter',
        });
        this.currentStep = 'updatePassword';
      },
      (error) => {
        console.error('Error verifying OTP:', error);

        if (error.status === 401) {
          this.handleError(error, 'Unauthorized access. Please verify your OTP and try again.');
        } else {
          this.handleError(error, 'Failed to verify OTP');
        }
      }
    );
  }

  updatePassword() {
    const newPasswordControl = this.forgotPasswordForm.get('newPassword');
    const confirmPasswordControl = this.forgotPasswordForm.get('confirmPassword');
    const countryCode = this.forgotPasswordForm.value.countryCode;
    const mobileNumber = this.forgotPasswordForm.value.mobileNumber;

    if (
      !newPasswordControl || newPasswordControl.value === '' ||
      !confirmPasswordControl || confirmPasswordControl.value === '' ||
      countryCode === '' || mobileNumber === ''
    ) {
      this.toast.error({
        detail: '',
        summary: 'New password, confirm password, country code, and mobile number cannot be empty',
        duration: 2000,
        position: 'topCenter',
      });
      return;
    }

    const newPassword = newPasswordControl.value;
    const confirmPassword = confirmPasswordControl.value;
    const fullPhoneNumber = countryCode + mobileNumber;

    if (newPassword !== confirmPassword) {
      this.toast.error({
        detail: '',
        summary: 'Passwords do not match',
        duration: 2000,
        position: 'topCenter',
      });
      return;
    }

    this.forgotPasswordService.updatePassword(fullPhoneNumber, newPassword).subscribe(
      () => {
        this.toast.success({
          detail: '',
          summary: 'Password updated successfully. Login now!!!',
          duration: 2000,
          position: 'topCenter',
        });
        this.router.navigate(['/login']);
      },
      (error) => {
        console.error('Error updating password:', error);
        this.handleError(error, 'Failed to update password');
      }
    );
  }


 private passwordMatchValidator(formGroup: FormGroup): ValidationErrors | null {
  const newPasswordControl = formGroup.get('newPassword');
  const confirmPasswordControl = formGroup.get('confirmPassword');

  if (!newPasswordControl || !confirmPasswordControl) {
    return null;
  }

  const newPassword = newPasswordControl.value;
  const confirmPassword = confirmPasswordControl.value;

  if (newPassword !== confirmPassword) {
    confirmPasswordControl.setErrors({ passwordMismatch: true });
  } else {
    confirmPasswordControl.setErrors(null);
  }

  return null;
}
private mobileNumberValidator(control: AbstractControl): ValidationErrors | null {
  const value: string = control.value || '';
  const isValid = /^\d{10}$/.test(value);

  return isValid ? null : { invalidMobileNumber: true };
}

  private passwordComplexityValidator() {
    return (control: AbstractControl): ValidationErrors | null => {
      const value: string = control.value || '';
      const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;

      if ((control.dirty || control.touched) && !regex.test(value)) {
        return { complexity: true };
      }

      return null;
    };
  }

  private handleError(error: any, defaultErrorMessage: string) {
    let errorMessage = defaultErrorMessage;

    if (error.status === 404) {
      errorMessage = 'User not found';
    } else if (error.status === 401) {
      errorMessage = 'Wrong OTP/ empty';
    } else if (error.error && error.error.message) {
      errorMessage = error.error.message;
    }

    this.toast.error({
      detail: '',
      summary: errorMessage,
      duration: 2000,
      position: 'topCenter',
    });
  }

}
