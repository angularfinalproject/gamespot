import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameRequestService {
  private apiUrl = 'http://localhost:8085';

  constructor(private http: HttpClient, private authService: AuthService) {}

  sendGameRequest(gameDetails: any, userEmail: string): Observable<any> {
    // Remove the user property from gameDetails
    delete gameDetails.user;

    // Include only userEmail in the request payload
    const requestData = {
      userEmail: userEmail,
      gameName: gameDetails.gameName,
      releaseYear: gameDetails.releaseYear,
      publisher: gameDetails.publisher
    };

    const apiUrl = `${this.apiUrl}/sendGameRequest/${userEmail}`;

    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post<any>(apiUrl, requestData, { headers: headers, responseType: 'text' as 'json' })
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.error('Error sending game request', error);
          throw error;
        })
      );
  }
}
