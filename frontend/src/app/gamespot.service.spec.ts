import { TestBed } from '@angular/core/testing';

import { GamespotService } from './gamespot.service';

describe('GamespotService', () => {
  let service: GamespotService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GamespotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
