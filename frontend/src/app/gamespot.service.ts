import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GamespotService {

  constructor(private http: HttpClient) {
    
  }

  customerLogin(email: string, password: string): Promise<any> {
    return this.http.get(`http://localhost:8085/signup/getsignupByEmailIdAndPassword/${email}/${password}`, { responseType: 'text' })
      .toPromise()
      .then(response => {
        console.log('Server response:', response);

        // Check if the response is defined and contains "Invalid email or password"
        if (response && response.includes("Invalid email or password")) {
          throw new Error("Invalid email or password");
        }

        // Check if the response is a success message
        if (response && response.includes("Signed in successfully")) {
          return { success: true, message: "Signed in successfully" };
        }

        // If neither error nor success, throw an unknown error
        throw new Error("Unknown error during login");
      })
      .catch(error => {
        console.error('Error during login:', error);
        throw error;
      });
  }
 // In your service method
// In your service method
customerSignup(customerDe: any): Observable<any> {
  return this.http.post('http://localhost:8085/signup', customerDe, { headers: { 'Content-Type': 'application/json' } });
}

getAllCountriesCodes():any{
  return this.http.get("https://restcountries.com/v3.1/all");
  }
}
