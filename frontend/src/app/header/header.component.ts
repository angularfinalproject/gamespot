// header.component.ts
import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(public authService: AuthService,private router: Router, private toast: NgToastService) {}

  logout() {

    this.authService.logout();
    this.toast.success({ detail: 'SUCESS', summary: 'Logged out successfully', duration: 2000, position: 'topCenter' });
    this.router.navigate(['']);

  }
}
