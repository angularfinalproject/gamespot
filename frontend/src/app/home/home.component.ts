// home.component.ts
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgToastService } from 'ng-angular-popup';
import { GameRequestService } from '../game-request.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  gameDetails: any = {};
  isFieldsDisabled = false;
  games = [
    { title: "Assasin's Creed Mirage", description: "Base Game", image: "../../assets/images/game_1.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Spider man 2", description: "Base Game", image: "../../assets/images/game_2.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Alan Wake 2", description: "Base Game", image: "../../assets/images/game_5.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Baldur's Gate 3", description: "Base Game", image: "../../assets/images/img_baldurgate_3.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Call of Duty: warzone", description: "Base Game", image: "../../assets/images/img_cod_mw2.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "GTA 5", description: "Base Game", image: "../../assets/images/img_gta5.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Elden Ring", description: "Base Game", image: "../../assets/images/img_eldenring.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Far Cry 6", description: "Base Game", image: "../../assets/images/img_farcry5.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "UFC 5", description: "Base Game", image: "../../assets/images/img_ufc5.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "The Crew: Moto Fest", description: "Base Game", image: "../../assets/images/img_thecrew.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "CyberPunk 2077", description: "Base Game", image: "../../assets/images/img_cyberpunk.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Godof War:Ragnarok", description: "Base Game", image: "../../assets/images/img_godofwar.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Sekiro:Shadows", description: "Base Game", image: "../../assets/images/img_sekiro.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Red Dead Redemption 2", description: "Base Game", image: "../../assets/images/game_8.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Mortal Kombat™ 1", description: "Base Game", image: "./../assets/images/game_4.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "The Last of us part -II", description: "Base Game", image: "./../assets/images/img_lastofus.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Battlefield 2042", description: "Base Game", image: "../../assets/images/img_battlefield2042.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },
    { title: "Dead Island 2", description: "Base Game", image: "../../assets/images/img_deadiland_2.png", discount: "-20%", originalPrice: "₹4500", salePrice: "3600" },



  ];

  chunkArray(array: any[], size: number): any[] {
    const result = [];
    for (let i = 0; i < array.length; i += size) {
      result.push(array.slice(i, i + size));
    }
    return result;
  }

  chunkedGames = this.chunkArray(this.games, 6);

  constructor(
    private fb: FormBuilder,
    private toast: NgToastService,
    private gameRequestService: GameRequestService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    // Retrieve user email from localStorage using the correct key
    const userEmail = localStorage.getItem('emailId');

    if (!userEmail) {
      // console.error('User email not found. User may not be logged in.');
      // Handle the case where user email is not found
    } else {
      // Set the userEmail in gameDetails
      this.gameDetails.userEmail = userEmail;
      console.log('User email retrieved from localStorage:', userEmail);
    }

    // Check authentication status on component initialization
    this.checkAuthentication();
  }

  checkAuthentication(): void {
    this.isFieldsDisabled = !this.authService.isAuthenticated();
  }

  handleFieldClick(): void {
    if (this.isFieldsDisabled) {
      this.findGames();
    }
  }

  findGames(): void {
    // Check if the user is authenticated before navigating
    if (this.authService.isAuthenticated()) {
      // Navigate to the desired page using the Angular Router
      this.router.navigate(['/filter']);
    } else {
      // Show toaster message if the user is not authenticated
      this.toast.error({
        detail: 'error',
        summary: 'Please register or log in to find games.',
        duration: 2000,
        position: 'topCenter'
      });
    }
  }

  sendGameRequest(): void {
    // Check if the user is authenticated before making the request
    if (!this.authService.isAuthenticated()) {
      this.toast.error({
        detail: 'ERROR',
        summary: ' Please register or log in to send a Game Request .',
        duration: 2000,
        position: 'topCenter'
      });
      // Handle the case where the user is not authenticated
      return;
    }

    // Check if required properties are not null
    if (!this.gameDetails.gameName || !this.gameDetails.releaseYear || !this.gameDetails.publisher) {
      this.toast.error({
        detail: 'ERROR',
        summary: 'Some required fields are empty. Please fill in all required fields.',
        duration: 2000,
        position: 'topCenter'
      });
      // Handle the case where required fields are null
      return;
    }

    // Continue with the game request, including the userEmail in the request payload
    this.gameRequestService.sendGameRequest({
      gameName: this.gameDetails.gameName,
      releaseYear: this.gameDetails.releaseYear,
      publisher: this.gameDetails.publisher
    }, this.gameDetails.userEmail).subscribe(
      (response: any) => {
        console.log('Game request response:', response);

        // Handle the response as needed
        if (response.includes('Game request sent, saved, and email sent successfully!')) {
          // Success, you may want to show a success message or perform further actions
          this.toast.success({
            detail: 'SUCCESS',
            summary: 'Game request sent successfully!',
            duration: 2000,
            position: 'topCenter'
          });
        } else {
          // Handle other response cases
          this.toast.error({
            detail: 'ERROR',
            summary: 'Failed to send game request. Please try again.',
            duration: 2000,
            position: 'topCenter'
          });
        }
      },
      (error) => {
        console.error('Error during game request:', error);
        // Handle error cases
        this.toast.error({
          detail: 'ERROR',
          summary: 'An error occurred during game request.',
          duration: 2000,
          position: 'topCenter'
        });
      }
    );
  }

  clearLocalStorage(): void {
    this.authService.logout();
    this.toast.error({
      detail: 'error',
      summary: 'Please register or log in to send a Gamer request.',
      duration: 2000,
      position: 'topCenter'
    });
  }
}
