import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { NgToastService } from 'ng-angular-popup';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  customer: any;
  code: any;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toast: NgToastService,
    private router: Router
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/)
        ]
      ],
      captcha: [this.generateRandomCode(), [Validators.required]]
    });
  }

  ngOnInit() {}

  generateRandomCode(): any {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let randomCode = '';
    for (let i = 0; i < 5; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      randomCode += characters.charAt(randomIndex);
    }
    this.code = randomCode;
  }

  async loginSubmit() {
    try {
      const enteredEmail = this.loginForm.value.email;
      const enteredPassword = this.loginForm.value.password;
      const enteredCaptcha = this.loginForm.value.captcha;





      // Check if email or password is empty
      if (!enteredEmail || !enteredPassword) {
        this.toast.error({
          detail: 'ERROR', summary: 'Email and Password cannot be empty',
          duration: 2000, position: 'topCenter'
        });
        return;
      }

      // Check captcha
      if (!enteredCaptcha || enteredCaptcha !== this.code) {
        this.toast.error({
          detail: 'ERROR', summary: enteredCaptcha ? 'Captcha is incorrect' : 'Captcha cannot be empty',
          duration: 2000, position: 'topCenter'
        });

        this.generateRandomCode();

        if (!enteredCaptcha) {
          return;
        }
      }


      const { email, password } = this.loginForm.value;

      this.authService.login(email, password).subscribe(
        (data: any) => {
          console.log('Server response:', data);

          if (typeof data === 'string' && data.includes('Invalid email or password')) {
            // handle invalid email or password
            this.toast.error({
              detail: 'ERROR', summary: 'Invalid email or password',
              duration: 2000, position: 'topCenter'
            });
          } else {
            // handle successful login
            this.customer = data;

            if (this.customer != null && enteredCaptcha === this.code) {
              this.authService.setAuthenticated(true);

              // Store the user's email in localStorage with the correct key ('userEmail')
              console.log('User email before storing:', this.loginForm.value.email);
          localStorage.setItem('emailId', this.loginForm.value.email);
          console.log('User email stored in localStorage:', this.loginForm.value.email);


              // Delay the navigation to give time for localStorage to update
              setTimeout(() => {
                this.toast.success({
                  detail: 'SUCCESS', summary: 'Logged in Successfully',
                  duration: 2000, position: 'topCenter'
                });

                this.router.navigate(['/home']);
              }, 100);
            } else {
              this.toast.error({
                detail: 'ERROR', summary: ' Incorrect Captcha ',
                duration: 2000, position: 'topCenter'
              });
            }
          }
        },
        (error) => {
          console.error('Error during login:', error);
          this.toast.error({
            detail: 'ERROR', summary: 'Invalid email or password',
            duration: 2000, position: 'topCenter'
          });
        }
      );


    } catch (error) {
      console.error('Error during login:', error);
      this.toast.warning({
        detail: 'ERROR', summary: 'Email, Password, and Captcha fields cannot be empty!!',
        duration: 2000, position: 'topCenter'
      });



    }
  }

  passwordMeetsRequirements(): boolean {
    return (
      this.passwordContainsLowercase() &&
      this.passwordContainsUppercase() &&
      this.passwordContainsDigit() &&
      this.passwordContainsSymbol()
    );
  }

  passwordContainsLowercase(): boolean {
    const passwordControl = this.loginForm.get('password');
    return passwordControl?.value && /[a-z]/.test(passwordControl.value);
  }

  passwordContainsUppercase(): boolean {
    const passwordControl = this.loginForm.get('password');
    return passwordControl?.value && /[A-Z]/.test(passwordControl.value);
  }

  passwordContainsDigit(): boolean {
    const passwordControl = this.loginForm.get('password');
    return passwordControl?.value && /\d/.test(passwordControl.value);
  }

  passwordContainsSymbol(): boolean {
    const passwordControl = this.loginForm.get('password');
    return passwordControl?.value && /[@$!%*?&]/.test(passwordControl.value);
  }
}
