import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewYearSaleComponent } from './new-year-sale.component';

describe('NewYearSaleComponent', () => {
  let component: NewYearSaleComponent;
  let fixture: ComponentFixture<NewYearSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewYearSaleComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NewYearSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
