import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GamespotService } from '../gamespot.service';
import { NgToastService } from 'ng-angular-popup';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  passwordsDoNotMatch: boolean = false;
  confirmPasswordDirty: boolean = false;
  countries: any;

  constructor(
    private fb: FormBuilder,
    private gamespotService: GamespotService,
    private router: Router,
    private toast: NgToastService,
  ) {
    this.registerForm = this.fb.group({
      fullName: ['', [Validators.required, Validators.minLength(3)]],

      emailId: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/)
        ]
      ],
      confirmPassword: ['', Validators.required],
      mobileNumber: ['', [
        Validators.required,
        Validators.pattern(/^[6-9]\d{9}$/),
        Validators.maxLength(10)
      ]],
      countryCode: ['', [Validators.required]]
    }, { validators: this.passwordMatchValidator });
  }

  ngOnInit() {
    // Fetch country codes when the component initializes
    this.gamespotService.getAllCountriesCodes().subscribe((data: any) => {
      this.countries = data;
    });
  }

  regSubmit(): void {
    const countryControl = this.registerForm.get('countryCode');
    const mobileNumberControl = this.registerForm.get('mobileNumber');

    if (countryControl && mobileNumberControl) {
      const countryCode: string = countryControl.value;
      const mobileNumber: string = mobileNumberControl.value;

      // Include the country code in the mobile number
      const fullPhoneNumber = countryCode + mobileNumber;

      // Update the data to include the full phone number
      const formData = { ...this.registerForm.value, mobileNumber: fullPhoneNumber };

      // Continue with your existing code for form submission
      if (this.registerForm.invalid) {
        this.toast.error({ detail: 'ERROR', summary: 'Fields cannot be empty or contain errors', duration: 2000, position: 'topCenter' });
        return;
      }

      if (this.passwordsDoNotMatch) {
        this.toast.error({ detail: 'ERROR', summary: 'Passwords do not match', duration: 2000, position: 'topCenter' });
        return;
      }

      this.gamespotService.customerSignup(formData).subscribe(
        (response) => {
          console.log('Server response:', response);

          if (response && response.message === 'Signup successful!') {
            console.log('Registration successful!');
            this.toast.success({ detail: 'SUCCESS', summary: 'Registration successful', duration: 2000, position: 'topCenter' });
            this.router.navigate(['/login']);
          } else {
            console.error('Unexpected server response:', response);
            this.toast.error({ detail: 'ERROR', summary: 'Unexpected server response', duration: 2000, position: 'topCenter' });
          }
        },
        (error) => {
          console.error('Error during registration:', error);

          if (error.status === 409) {
            console.log('Email already exists in the database.');
            this.toast.error({ detail: 'ERROR', summary: 'Email already exists in the database', duration: 2000, position: 'topCenter' });
          } else if (error.error instanceof Blob && error.headers.get('content-type')?.toLowerCase()
            .includes('application/json')) {
            const reader = new FileReader();
            reader.onload = () => {
              const errorResponse = JSON.parse(reader.result as string);
              console.error('Server error response:', errorResponse);
            };
            reader.readAsText(error.error);
          } else {
            this.toast.error({ detail: 'ERROR', summary: 'Error during registration', duration: 2000, position: 'topCenter' });
          }
        }
      );

    } else {
      // Handle the case where either control is not found
    }
  }

  passwordMatchValidator(formGroup: FormGroup) {
    const passwordControl = formGroup.get('password');
    const confirmPasswordControl = formGroup.get('confirmPassword');

    if (!passwordControl || !confirmPasswordControl) {
      return null;
    }

    if (passwordControl.value !== confirmPasswordControl.value) {
      confirmPasswordControl.setErrors({ passwordMismatch: true });
    } else {
      confirmPasswordControl.setErrors(null);
    }

    return null;
  }

  checkPasswordMatch(): void {
    const confirmPasswordControl = this.registerForm.get('confirmPassword');
    if (confirmPasswordControl) {
      this.passwordsDoNotMatch = confirmPasswordControl.value !== this.registerForm.get('password')?.value;
      this.confirmPasswordDirty = true;
    }
  }

  passwordContainsLowercase(): boolean {
    const passwordControl = this.registerForm.get('password');
    return passwordControl?.value && /[a-z]/.test(passwordControl.value);
  }

  passwordContainsUppercase(): boolean {
    const passwordControl = this.registerForm.get('password');
    return passwordControl?.value && /[A-Z]/.test(passwordControl.value);
  }

  passwordContainsDigit(): boolean {
    const passwordControl = this.registerForm.get('password');
    return passwordControl?.value && /\d/.test(passwordControl.value);
  }

  passwordContainsSymbol(): boolean {
    const passwordControl = this.registerForm.get('password');
    return passwordControl?.value && /[@$!%*?&]/.test(passwordControl.value);
  }
}